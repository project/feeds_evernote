<?php
/**
 */
class EvernoteParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();
    $raw = $fetcher_result->getRaw();
    if (!empty($raw)) {
      $config = $raw['config'];    
      $notes = $raw['notes']->notes;
      feeds_evernote_includes();
      foreach ($notes as $note) {
        $note_info = $raw['note_store']->getNote($config['auth_token'], $note->guid, 1, 1, 1, 1);
        
        $tags = array();
        foreach ($note_info->tagGuids as $tag_guid) {
          $tag = $raw['note_store']->getTag($config['auth_token'], $tag_guid);
          $tags[] = $tag->name;
        }
        
        $files = feeds_evernote_get_files($note_info, $config);
        
        $result->items[] = array(
          'title' => $note_info->title,
          'content' => feeds_evernote_sanitize_body($note_info->content),
          'created' => ($note_info->created / 1000), //milliseconds to seconds
          'guid' => $note_info->guid,
          'source_url' => $note_info->attributes->sourceURL,
          'updateSequenceNum' => $note_info->updateSequenceNum,
          'tags' => $tags,
          'image' => $files['image'],
          'files' => $files['file'],
        );
      }
    }
    return $result;
  }

  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the feed item.'),
      ),
      'content' => array(
        'name' => t('Content'),
        'description' => t('Content of the feed item.'),
      ),
      'created' => array(
        'name' => t('Published date'),
        'description' => t('Published date as UNIX time GMT of the feed item.'),
      ),
      'source_url' => array(
        'name' => t('Source URL'),
        'description' => t('Source URL of the '),
      ),
      'guid' => array(
        'name' => t('Item GUID'),
        'description' => t('Global Unique Identifier of the feed item.'),
      ),
      'tags' => array(
        'name' => t('Tags'),
        'description' => t('An array of categories that have been assigned to the feed item.'),
      ),
      'image' => array(
        'name' => t('Image'),
        'description' => t('Images'),
      ),
      'file' => array(
        'name' => t('File'),
        'description' => t('Files'),
      ),
     ) + parent::getMappingSources();
  }
}
