<?php

/**
 * @file
 */

/**
 * Definition of EvernoteFetcher.
 */
class EvernoteFetcherResult extends FeedsFetcherResult {
  protected $config;

  /**
   * Constructor.
   */
  public function __construct($config) {
    $this->config = $config;
    parent::__construct('');
  }

  /**
   * Implementation of getRaw();
   */
  public function getRaw() {
    //authenticate the config to get auth_token
    $this->config = feeds_evernote_authenticate($this->config);
     
    if ($this->config['auth_token']) {
      $noteStore = feeds_evernote_note_store($this->config['shard_id']);

      //build any filters found in config
      $filter = new edam_notestore_NoteFilter();
      if ($this->config['notebook_guid'] != '') {
        $filter->notebookGuid = $this->config['notebook_guid'];
      }
      if ($this->config['search_guid'] != '') {
        $search = $noteStore->getSearch($this->config['auth_token'], $this->config['search_guid']);
        $filter->words = $search->query;
      }
      if ($this->config['tag_guid'] != '') {
        $filter->tagGuids = array($this->config['tag_guid']);
      }
      
      //fetch notes based on filters and auth
      $notes = $noteStore->findNotes($this->config['auth_token'], $filter, 0, 9999);
      
      //this array will be used by EvernoteParser
      return array('notes' => $notes, 'config' => $this->config, 'note_store' => $noteStore);
    }
    return array();
  }
}

class EvernoteFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $config = array_merge($this->config, $source->getConfigFor($this));
    return new EvernoteFetcherResult($config);
  }
  
  public function sourceForm($source_config) {
    $config = array_merge($this->config, $source_config);
    $form = $this->settingsForm($config, 'sourceForm'); 
    $form['blank'] = array('#type' => 'hidden', '#default_value' => 'blank',);  //the source form requires at least one form element   
    return $form; 
  }
  
  public function sourceFormValidate(&$values) {
    $values = array_merge($this->config, $values);
    $values['filter_allow_override'] = $values['auth_allow_override'] = ''; //these should never be saved by the source form
    $this->settingsValidate($values);
  }
  
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'username' => '',
      'password' => '',
      'password_encrypt' => '',
      'shard_id' => '',
      'notebook_name' => '',
      'notebook_guid' => '',
      'search_name' => '',
      'search_guid' => '',
      'tag_name' => '',
      'tag_guid' => '',
      'auth_allow_override' => '',
      'filter_allow_override' => '',
    );
  }

  /**
   * Config form.
   */
  public function configForm(&$form_state) {
    return $this->settingsForm($this->config, 'configForm');
  }

  public function configFormValidate(&$values) {
    $this->settingsValidate($values);
  }
  
  protected function settingsForm($config, $form_name) {
    $form = array();
  
    /*
    feeds_evernote_includes();
    $request = new OAuth_SimpleRequest(EVERNOTE_REQUEST_TOKEN_URL, EVERNOTE_CONSUMER_KEY, EVERNOTE_CONSUMER_SECRET);
    $request->sendRequest();
    $response = $request->getResponseStruct();
    $_SESSION['requestToken'] = $response['oauth_token'];
    */
    
    if ('configForm' == $form_name || $config['auth_allow_override']) {
      $form['username'] = array(
        '#prefix' => '<h2>Evernote Authentication</h2>',
        //'#prefix' => '<div><label>Authenticate</label><p>Before we can pull data from your account, we need you to authenticate with Evernote. This authentication will last for 1 year, after which you will need to log in again.</p><iframe name="iframe1" src="'. EVERNOTE_SPHOSTNAME .'/OAuth.action?oauth_callback='. url('admin/evernote/authenticate/'. $action, array('absolute' => TRUE)) .'&oauth_token='. $_SESSION['requestToken'] . $id_query_string .'&format=microclip" frameborder="1" scrolling="no" width="400" height="200"></iframe></div>',
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#default_value' => $config['username'],
        '#description' => t('Your Evernote username'),
      );
      
      $pass_desc = t('ATTENTION: Your password will be saved to the database in plain text.  This is not recommended.  Install the !module_link module to enable password encryption for Evernote Fetcher.', array('!module_link' => l('Encryption', 'http://drupal.org/project/encrypt')));
      if (module_exists('encrypt')) {
        $pass_desc = t('Your Evernote password will be stored in an encrypted format (encryption method: !format)', array('!format' => variable_get('encrypt_default_method', 'none')));
      }
      $form['password'] = array(
        '#type' => 'password',
        '#title' => t('Password'),
        '#default_value' => '',
        '#description' => $pass_desc,
      );
      
      if ('configForm' == $form_name) {
        $form['auth_allow_override'] = array(
          '#type' => 'checkbox',
          '#title' => t('Allow authentication settings to be overridden on source form'),
          '#default_value' => $config['auth_allow_override'],
        ); 
      }
    }
    
    if ('configForm' == $form_name || $config['filter_allow_override']) {
      $form['notebook_name'] = array(
        '#prefix' => '<h2>Evernote Filters</h2>',
        '#type' => 'textfield',
        '#title' => t('Notebook name'),
        '#default_value' => $config['notebook_name'],
        '#description' => t('Filter the notes that will be imported to a specific Evernote notebook'),
      );
      
      $form['search_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Evernote search'),
        '#default_value' => $config['search_name'],
        '#description' => t('Filter the notes that will be imported to a specific Evernote saved search'),
      ); 
      
      $form['tag_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Evernote tag'),
        '#default_value' => $config['tag_name'],
        '#description' => t('Filter the notes that will be imported to a specific Evernote tag'),
      ); 
      
      if ('configForm' == $form_name) {
        $form['filter_allow_override'] = array(
          '#type' => 'checkbox',
          '#title' => t('Allow filter settings to be overridden on source form'),
          '#default_value' => $config['filter_allow_override'],
        );
      }
    }
    return $form;    
  }

  protected function settingsValidate(&$values) {
    $values = feeds_evernote_authenticate($values);
    if (isset($values['auth_token'])) {      
      if (module_exists('encrypt')) {
        $key = 'feeds_evernote_encrypt_' . $values['username'];
        variable_set($key, encrypt($values['password'])); //we use the variable table because it stores as a blob and encrypted strings can't be stored in a db text field 
        $values['password_encrypt'] = $key;
        $values['password'] = '';
      }
      
      $noteStore = feeds_evernote_note_store($values['shard_id']);
      $filters = array('notebook', 'search', 'tag');   
      foreach ($filters as $filter) {
        if ($values[$filter . '_name']) {
          $values[$filter . '_guid'] = '';
          
          //fetch filter options
          switch ($filter) {
            case 'notebook': $items = $noteStore->listNotebooks($values['auth_token']); break;
            case 'search': $items = $noteStore->listSearches($values['auth_token']); break;
            case 'tag': $items = $noteStore->listTags($values['auth_token']); break;
          }
          
          //look for an option that matches the submitted name and store the guid
          foreach($items as $item) {
            if ($item->name == $values[$filter . '_name']) {
              $values[$filter . '_guid'] = $item->guid;
            } 
          }
          
          //if no option was found, throw a form error for the submitted name
          if (empty($values[$filter . '_guid'])) {
            $values[$filter . '_name'] = '';
            $form_key = $filter . '_name'; //@TODO get right form key
            form_set_error($form_key, t('Please enter a valid !filter name for %username.', array('!filter' => $filter, '%username' => $values['username'])));
          }
        }
      }
    }
  }
}



